---
layout: cv
permalink: /cv/
---

# CV - Omry Cohen

<br>
{{ site.time | date: '%B %d, %Y' }}

## Experience

### May 2024 - Now

- Operations researcher
- Israeli Air Force
- Tel Aviv

### Aug 2022 - May 2024

- Algorithm developer
- Unit 8200, Israeli Intelligence Corps
- Tel Aviv

### Nov 2019 - Aug 2022

- Fellow physicist researcher
- Israeli Ministry of Defense
- Jerusalem

### Jul 2016 - Nov 2019

- Talpiot program cadet
- Israel Defense Forces
- Jerusalem

## Education

### 2019 - 2021

- MSc (Physics) at the Hebrew University of Jerusalem, Israel (HUJI)
- _Learning Curves for Deep Neural Networks - A Field Theory Approach_
- Supervised by Dr. Zohar Ringel
- _Magna cum laude_

### 2016 - 2019

- BSc (Mathematics & Physics) at the Hebrew University of Jerusalem, Israel (HUJI)
- As a part of Talpiot program
- _Summa cum laude_

## Awards

### 2016

- Silver medal (63rd overall rank)
- [47th International Physics Olympiad (IPhO)](https://ipho-unofficial.org/timeline/2016/individual)
- Zurich, Switzerland

### 2016

- Honorable mention (93rd overall rank)
- [17th Asian Physics Olympiad (APhO)](http://apho2016.ust.hk/files/APhO%202016%20result.pdf)
- Hong Kong
